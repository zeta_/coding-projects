import qualified Data.Map as Map
import Data.Char

stringToDigits :: String -> [Int]
stringToDigits = map digitToInt . filter isDigit

phoneBook =
  [
    ("bob", "834-3275")
  , ("bob", "8921-1363")
  , ("bob", "834-1763")
  , ("jenny", "862-1212")
  , ("kurt", "321-4589")
  , ("kurt", "897-4529")
  , ("kurt", "323-8799")
  , ("kurt", "111-8888")
  , ("john", "862-1342")
  , ("dillon", "54K-3254")
  , ("rick", "657-1111")
  ]

phoneBookToMap :: (Ord k) => [(k, a)] -> Map.Map k [a]
phoneBookToMap xs = Map.fromListWith (++) $ map (\ (k,v) -> (k, [v])) xs
