import Data.List

uniqueElements :: (Eq a) => [a] -> Int
uniqueElements = length . nub
