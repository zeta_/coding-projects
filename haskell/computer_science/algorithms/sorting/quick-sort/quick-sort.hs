quickSort :: (Ord a) => [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
  let
    lessOrEqual = quickSort [a | a <- xs, a <= x]
    greater = quickSort [a | a <- xs, a > x]
  in
    lessOrEqual ++ [x] ++ greater
