module Main where

import Control.Applicative

f :: Int -> Int -> Int
f x y = (2 * x) + y
main :: IO ()
main =
  do
    print (show $ f <$> Just 5 <*> Just 10)
