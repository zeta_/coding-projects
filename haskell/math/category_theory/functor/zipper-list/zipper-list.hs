module Main where

data List a = Empty | Cons a (List a) deriving (Show, Read, Eq, Ord)
type ZipperList a = ([a],[a])

goForward :: ZipperList a -> ZipperList a
goForward (x:xs, ys) = (xs, x:ys)

goBack :: ZipperList a -> ZipperList a
goBack (xs, y:ys) = (y:xs, ys)

main :: IO ()
main =
  do
    let listExpression = [1,2,3,4,5]
    print (goForward (listExpression, []))
    print (goBack ([5], [4,3,2,1]))
