module Main where

import Data.Char
import Data.List

main :: IO ()
main =
  do
    result <- fmap (intersperse '-' . reverse . map toUpper) getLine
    putStrLn result
