f :: [Int] -> [Int]
f (_:x:xs) = x:(f xs)
f _ = []
