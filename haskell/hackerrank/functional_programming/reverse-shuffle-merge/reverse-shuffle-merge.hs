import Data.List (permutations)
merge :: String -> String -> [String]
merge xs [] = []
merge [] ys = []
merge (x:xs) (y:ys) = x : y : merge xs ys

reverse "abc"
