module Main where

helloWorlds :: Int -> IO ()
helloWorlds n = putStrLn $ unlines (replicate n "Hello World")

main :: IO ()
main =
  do
    n <- readLn
    helloWorlds n
