rev :: [Int] -> [Int]
rev [] = []
rev (h:t) = rev t ++ [h]
