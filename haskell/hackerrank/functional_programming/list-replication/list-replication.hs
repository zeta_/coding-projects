f :: Int -> [Int] -> [Int]
f n array = concatMap (replicate n) array
