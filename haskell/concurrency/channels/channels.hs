module Main where

import Control.Concurrent
import Control.Concurrent.STM

main :: IO ()
main =
  do
    messages <- atomically newTQueue

    forkIO $ atomically $ writeTQueue messages "ping"

    message <- atomically $ readTQueue messages
    putStrLn message
