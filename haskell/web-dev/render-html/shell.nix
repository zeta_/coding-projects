let
  pkgs = import <nixpkgs> {};
in
with pkgs;
mkShell
  {
    buildInputs = with pkgs;
      [
        (haskell.packages.ghc865.ghcWithHoogle (hpkgs: with hpkgs;
          [
            ghcid
            cabal-install
            stack

            yesod
            yesod-bin
            yesod-static
            yesod-form
            persistent-sqlite
          ]))
        
        # ghcide-nix installation:
        (import (builtins.fetchTarball "https://github.com/cachix/ghcide-nix/tarball/master") {}).ghcide-ghc865
      ]; # end of buildInputs    

    shellHook =
      ''
         export PS1='\n\[\033[1;32m\][\[\e]0;nix-shell: \W\a\]nix-shell:/\W]\$ \[\033[0m\]'
         export EDITOR="$(emacs)"

         export HIE_HOOGLE_DATABASE="$(cat $(which hoogle) | sed -n -e 's|.*--database \\(.*\\.hoo\\).*|\\1|p')";
         export NIX_GHC="$(which ghc)";
         export NIX_GHCPKG="$(which ghc-pkg)";
         export NIX_GHC_DOCDIR="$NIX_GHC/../../share/doc/ghc/html";
         export NIX_GHC_LIBDIR="$(ghc --print-libdir)";
      '';
  } # end of mkShell





