{-#
LANGUAGE OverloadedStrings
       , QuasiQuotes
       , TemplateHaskell
       , TypeFamilies
#-}

import Yesod
import Data.Text
import qualified Data.Text.IO as TIO

data Code = Code

mkYesod "Code" [parseRoutes|
/ HomeR GET
|]

instance Yesod Code

htmlFilePath :: FilePath
htmlFilePath = "/home/zeta/coding-projects/haskell/web-dev/render-html/code.html"

getHomeR :: Handler Html
getHomeR =
  do
    htmlContent <- liftIO $ TIO.readFile htmlFilePath
    return $ preEscapedToMarkup htmlContent

main :: IO ()
main = warp 3000 Code

