let
  pkgs = import <nixpkgs> {};
in
with pkgs;
mkShell
  {
    buildInputs = with pkgs;
      [
        elm2nix
        
        (with elmPackages;
          [
            elm
            elm-language-server
            
            elm-format
            elm-instrument
            elmi-to-json
            elm-analyse

            elm-coverage
            elm-doc-preview
            elm-live
            elm-test
            elm-upgrade
            elm-verify-examples
            elm-xref                        
          ])

        (with nodePackages;
          [
            node2nix
            nodejs-12_x

            bash-language-server
            dockerfile-language-server-nodejs
            eslint
            typescript
            typescript-language-server
            vscode-css-languageserver-bin
            vscode-html-languageserver-bin
          ])

        
      ]; # end of buildInputs

    # shellHook ='''';
  }
#-------------end of mkShell------------------------------------





